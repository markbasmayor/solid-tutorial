<?php

interface Area
{
    public function getArea();
}

interface Perimeter
{
    public function getPerimeter();
}

interface Quadrilateral
{
    public function getWidth();
    public function getHeight();
}

class Square implements Area, Perimeter, Quadrilateral {}
class Rectangle implements Area, Perimeter, Quadrilateral {}
class Triangle implements Area, Perimeter {}
