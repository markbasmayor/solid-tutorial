<?php

class Phone
{
    protected $brand = 'Nokia';
    protected $model = '3310';

    public function getBrand()
    {
        return $this->brand;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function order()
    {
        // Place order in Amazon
    }
}

$phone = new Phone();
$phone->order();

