<?php

interface Shape
{
    public function getArea();
    public function getPerimeter();
    public function getWidth();
    public function getHeight();
}

class Square implements Shape {}
class Rectangle implements Shape {}
class Triangle implements Shape {}
