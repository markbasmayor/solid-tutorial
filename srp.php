<?php

class Phone
{
    protected $brand = 'Nokia';
    protected $model = '3310';

    public function getBrand()
    {
        return $this->brand;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function printBootupScreen()
    {
        echo 'The all new '.$this->getBrand()
            .PHP_EOL.$this->getModel();
    }
}

$phone = new Phone();
$phone->printBootupScreen();

