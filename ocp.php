<?php
class Collection
{
    protected $elements = array();

    public function __construct(array $elements)
    {
        $this->elements = $elements;
    }

    public function getElements()
    {
        return $this->elements;
    }
}

class Sorter
{
    protected $collection;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function sort()
    {
        $elements = $this->collection->getElements();
        $sorted = sort($elements);
        return $elements;
    }
}

$collection =  new Collection([5,4,1,3]);
$sorter = new Sorter($collection);
$sorted = $sorter->sort();
var_dump($sorted);



