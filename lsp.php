<?php

class Line2D
{
    protected $x1;
    protected $y1;
    protected $x2;
    protected $y2;

    public function __construct($x1, $y1, $x2, $y2)
    {
        $this->x1 = $x1;
        $this->x2 = $x2;
        $this->y1 = $y1;
        $this->y2 = $y2;
    }

    public function getLength()
    {
        return sqrt(($this->x2 - $this->x1)^2 + ($this->y2 - $this->y1)^2);
    }
}

class Line3D extends Line2D
{
    protected $z1;
    protected $z2;

    public function __construct($x1, $y1, $x2, $y2, $z1, $z2)
    {
        $this->x1 = $x1;
        $this->x2 = $x2;
        $this->y1 = $y1;
        $this->y2 = $y2;
        $this->z1 = $z1;
        $this->z2 = $z2;
    }
}
class LinePlotter
{
    protected $line;

    public function __construct(Line2D $line)
    {
        $this->line = $line;
    }

    public function plot()
    {
        echo $this->line->getLength();
    }
}

$line2D = new Line2D(1,1,4,4);
$plotter2D = new LinePlotter($line2D);
$plotter2D->plot();

$line3D = new Line2D(1,1,4,4,5,5);
$plotter3D = new LinePlotter($line3D);
$plotter3D->plot();

