<?php

class Rectangle
{
    protected $width;
    protected $height;

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getArea()
    {
        return $this->height * $this->width;
    }
}

class Square extends Rectangle
{
    public function setWidth($side)
    {
        $this->width = $side;
        $this->height = $side;
    }

    public function setHeight($side)
    {
        $this->width = $side;
        $this->height = $side;
    }
}

class AreaVerifier
{
    public function verify(Rectangle $r)
    {
        $r->setWidth(5);
        $r->setHeight(4);
        if ($r->getArea() != 20) {
            throw new LogicException('Bad area');
        }
        return true;
    }
}

$verifier = new AreaVerifier();
$verifier->verify(new Rectangle());
$verifier->verify(new Square());
