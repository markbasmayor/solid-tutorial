<?php

abstract class Shape
{
    abstract function getArea();
    abstract function getPerimeter();
    abstract function getWidth();
    abstract function getHeight();
}

class Square extends Shape {}
class Rectangle extends Shape {}
class Triangle extends Shape {}
