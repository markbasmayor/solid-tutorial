<?php

class CopyModule
{

    protected $keyboardReader;
    protected $printerWriter;
    protected $data;

    public function readData()
    {
        $this->data = $this->keyboardReader->getInput();
    }

    public function writeData()
    {
        $this->printerWriter->writeData($this->data);
    }
}
